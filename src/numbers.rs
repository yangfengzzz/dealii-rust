/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */


use crate::types::*;

/**
 * Representation of the largest number that can be put into an unsigned
 * integer. This value is widely used throughout the library as a marker for
 * an invalid unsigned integer value, such as an invalid array index, an
 * invalid array size, and the like.
 */
pub const INVALID_UNSIGNED_INT: u32 = 1_u32.overflowing_neg().0;

/**
 * Representation of the largest number that can be put into a size_type.
 * This value is used throughout the library as a marker for an invalid
 * size_type value, such as an invalid array index, an invalid array size,
 * and the like. Invalid_size_type is equivalent to INVALID_DOF_INDEX.
 */
pub const INVALID_SIZE_TYPE: GlobalDofIndex = (1 as GlobalDofIndex).overflowing_neg().0;

/**
 * An invalid value for indices of degrees of freedom.
 */
pub const INVALID_DOF_INDEX: GlobalDofIndex = (1 as GlobalDofIndex).overflowing_neg().0;

/**
 * An invalid value for coarse cell ids. See the glossary
 * entry on
 * @ref GlossCoarseCellId "coarse cell IDs"
 * for more information.
 */
pub const INVALID_COARSE_CELL_ID: CoarseCellId = (1 as CoarseCellId).overflowing_neg().0;

/**
 * Invalid material_id which we need in several places as a default value.
 * We assume that all material_ids lie in the range [0,
 * INVALID_MATERIAL_ID).
 */
pub const INVALID_MATERIAL_ID: MaterialId = (1 as MaterialId).overflowing_neg().0;

/**
 * Invalid boundary_id which we need in several places as a default value.
 * We assume that all valid boundary_ids lie in the range [0,
 * INVALID_BOUNDARY_ID).
 *
 * @see
 * @ref GlossBoundaryIndicator "Glossary entry on boundary indicators"
 */
pub const INVALID_BOUNDARY_ID: BoundaryId = (1 as BoundaryId).overflowing_neg().0;

/**
 * A boundary indicator number that we reserve for internal faces.  We
 * assume that all valid boundary_ids lie in the range [0,
 * INTERNAL_FACE_BOUNDARY_ID).
 *
 * This is an indicator that is used internally (by the library) to
 * differentiate between faces that lie at the boundary of the domain and
 * faces that lie in the interior of the domain. You should never try to
 * assign this boundary indicator to anything in user code.
 *
 * @see
 * @ref GlossBoundaryIndicator "Glossary entry on boundary indicators"
 */
pub const INTERNAL_FACE_BOUNDARY_ID: BoundaryId = (1 as BoundaryId).overflowing_neg().0;

/**
 * A manifold_id we reserve for the default flat Cartesian manifold.
 *
 * @see
 * @ref GlossManifoldIndicator "Glossary entry on manifold indicators"
 */
pub const FLAT_MANIFOLD_ID: ManifoldId = (1 as ManifoldId).overflowing_neg().0;

/**
 * A special id for an invalid subdomain id. This value may not be used as a
 * valid id but is used, for example, for default arguments to indicate a
 * subdomain id that is not to be used.
 *
 * See the
 * @ref GlossSubdomainId "glossary"
 * for more information.
 */
pub const INVALID_SUBDOMAIN_ID: SubdomainId = (1 as SubdomainId).overflowing_neg().0;

/**
 * The subdomain id assigned to a cell whose true subdomain id we don't
 * know, for example because it resides on a different processor on a mesh
 * that is kept distributed on many processors. Such cells are called
 * "artificial".
 *
 * See the glossary entries on
 * @ref GlossSubdomainId "subdomain ids"
 * and
 * @ref GlossArtificialCell "artificial cells"
 * as well as the
 * @ref distributed
 * module for more information.
 */
pub const ARTIFICIAL_SUBDOMAIN_ID: SubdomainId = (2 as SubdomainId).overflowing_neg().0;