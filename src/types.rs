/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

/**
 * The type used to denote subdomain_ids of cells.
 *
 * See the
 * @ref GlossSubdomainId "glossary"
 * for more information.
 *
 * There is a special value, numbers::INVALID_SUBDOMAIN_ID that is used to
 * indicate an invalid value of this type.
 */
pub type SubdomainId = u32;

/**
 * The type used for global indices of vertices.
 */
pub type GlobalVertexIndex = u64;

/**
 * The type used to denote the global index of degrees of freedom. This
 * type is then also used for querying the global *number* of degrees
 * of freedom, since the number is simply the largest index plus one.
 *
 * While in sequential computations the 4 billion indices of 32-bit unsigned
 * integers is plenty, parallel computations using (for example) the
 * parallel::distributed::Triangulation class can overflow this number and
 * consequently, deal.II chooses a larger integer type when
 * configured to use 64-bit indices.
 *
 * The data type always corresponds to an unsigned integer type.
 *
 * See the
 * @ref GlobalDoFIndex
 * page for guidance on when this type should or should not be used.
 */
pub type GlobalDofIndex = u32;

/**
 * The type used to denote the global index of a cell. This type
 * is then also used for querying the global *number* of cells in
 * a triangulation since the number is simply the largest index plus one.
 *
 * While in sequential computations the 4 billion indices of 32-bit unsigned
 * integers is plenty, parallel computations using (for example) the
 * parallel::distributed::Triangulation class can overflow this number and
 * consequently, deal.II chooses a larger integer type when
 * configured to use 64-bit indices.
 *
 * The data type always corresponds to an unsigned integer type.
 */
pub type GlobalCellIndex = u32;

/**
 * The type used for coarse-cell ids. See the glossary
 * entry on
 * @ref GlossCoarseCellId "coarse cell IDs"
 * for more information.
 */
pub type CoarseCellId = GlobalCellIndex;

/**
 * The type used to denote boundary indicators associated with every piece
 * of the boundary and, in the case of meshes that describe manifolds in
 * higher dimensions, associated with every cell.
 *
 * There is a special value, numbers::INTERNAL_FACE_BOUNDARY_ID that is used
 * to indicate an invalid value of this type and that is used as the
 * boundary indicator for faces that are in the interior of the domain and
 * therefore not part of any addressable boundary component.
 *
 * @see
 * @ref GlossBoundaryIndicator "Glossary entry on boundary indicators"
 */
pub type BoundaryId = u32;

/**
 * The type used to denote manifold indicators associated with every object
 * of the mesh.
 *
 * There is a special value, numbers::FLAT_MANIFOLD_ID that is used to
 * indicate the standard cartesian manifold.
 *
 * @see
 * @ref GlossManifoldIndicator "Glossary entry on manifold indicators"
 */
pub type ManifoldId = u32;

/**
 * The type used to denote material indicators associated with every cell.
 *
 * There is a special value, numbers::INVALID_MATERIAL_ID that is used to
 * indicate an invalid value of this type.
 *
 * @see
 * @ref GlossMaterialId "Glossary entry on material indicators"
 */
pub type MaterialId = u32;

/**
 * The type used to denote geometric entity types.
 */
pub type GeometricEntityType = u8;